import ProfilePage from "./ProfilePage";
import Calendar from "./userCalendar/Calendar";

const Dashboard = () => {
  return (
    <>
      <div className="user-dashboard grid grid-cols-12">
        <div className="TodoList col-span-3 bg-black"></div>
        <div className="Calendar col-span-7">
          <Calendar />
        </div>
        <div className="Profile col-span-2">
          <ProfilePage />
        </div>
      </div>
    </>
  );
};

export default Dashboard;
