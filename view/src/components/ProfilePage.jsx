import React, { useState, useEffect } from "react";
import Logo from "../assests/homesweat logo black.png";

const UserProfile = () => {
  const [user, setUser] = useState({
    name: "",
    height: "",
    weight: "",
    profilePic: "",
  });

  useEffect(() => {
    // Fetch user details from the database
    // You can replace this with your own logic to fetch data
    const fetchUserDetails = async () => {
      try {
        // Make an API call to fetch user details
        const response = await fetch("your-api-url");
        const userData = await response.json();

        // Set the user details in the state
        setUser(userData);
      } catch (error) {
        console.error("Error fetching user details:", error);
      }
    };

    fetchUserDetails();
  }, []);

  const handleChangeProfilePic = (e) => {
    // Handle profile picture change
    // You can replace this with your own logic to upload the image to the server
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        setUser((prevUser) => ({
          ...prevUser,
          profilePic: reader.result,
        }));
      };

      reader.readAsDataURL(file);
    }
  };

  return (
    <>
      <div className="logo flex justify-center items-center border-b-2 border-black p-4">
        <img src={Logo} alt="homesweat logo" className="h-8 text-black" />
      </div>
      <div>
        <h1>User Profile</h1>
        <div>
          <img
            src={user.profilePic}
            alt="Profile"
            style={{ width: "200px", height: "200px" }}
          />
          <input
            type="file"
            accept="image/*"
            onChange={handleChangeProfilePic}
          />
        </div>
        <div>
          <label>Name:</label>
          <span>{user.name}</span>
        </div>
        <div>
          <label>Height:</label>
          <span>{user.height}</span>
        </div>
        <div>
          <label>Weight:</label>
          <span>{user.weight}</span>
        </div>
      </div>
    </>
  );
};

export default UserProfile;
